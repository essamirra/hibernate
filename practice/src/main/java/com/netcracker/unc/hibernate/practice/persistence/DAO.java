package com.netcracker.unc.hibernate.practice.persistence;

import java.io.Serializable;

public interface DAO<T> {

    Serializable create(T instance);

    T read(Serializable id);

    void update(T transientObject);

    void delete(T persistentObject);

}
