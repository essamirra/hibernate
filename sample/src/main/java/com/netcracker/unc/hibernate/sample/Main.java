package com.netcracker.unc.hibernate.sample;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        User user = new User(1, "Levi");

        SessionFactory sessionFactory = new Configuration().configure()
                .buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();

        // now lets pull events from the database and list them
        session = sessionFactory.openSession();
        session.beginTransaction();
        List<User> result = (List<User>) session.createQuery("from User").list();
        for (User event : result) {
            System.out.println(event);
        }
        session.getTransaction().commit();
        session.close();
    }
}
