package com.netcracker.unc.hibernate.practice.datamodel;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
public class Scholarship extends BaseEntity {

    private Student granted;

    private Integer amount;

    @Temporal(TemporalType.DATE)
    private Date from;

    @Temporal(TemporalType.DATE)
    private Date to;

    public Scholarship() {
    }


    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Student getGranted() {
        return granted;
    }

    public void setGranted(Student granted) {
        this.granted = granted;
    }

    @Column
    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Column(name = "fromDate")
    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    @Column
    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    @Override
    public String toString() {
        return "Scholarship{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", granted=" + granted +
                ", amount=" + amount +
                ", from=" + from +
                ", to=" + to +
                '}';
    }
}
