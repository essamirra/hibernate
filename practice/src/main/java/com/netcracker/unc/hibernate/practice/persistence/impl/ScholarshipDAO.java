package com.netcracker.unc.hibernate.practice.persistence.impl;

import com.netcracker.unc.hibernate.practice.datamodel.Scholarship;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.io.Serializable;

public class ScholarshipDAO extends GenericDAOImpl<Scholarship> {

    public ScholarshipDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public Scholarship read(Serializable id) {
        Transaction transaction = null;
        Session session = sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            Scholarship object = session.get(Scholarship.class, id);
            session.getTransaction().commit();
            return object;

        } catch (RuntimeException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new RuntimeException(e.getClass() + ": cannot read instance with" + id.toString());
        } finally {

            session.close();
        }
    }
}
