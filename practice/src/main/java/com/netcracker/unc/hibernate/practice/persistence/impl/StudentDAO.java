package com.netcracker.unc.hibernate.practice.persistence.impl;

import com.netcracker.unc.hibernate.practice.datamodel.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.io.Serializable;
import java.util.List;

public class StudentDAO extends GenericDAOImpl<Student> {

    public StudentDAO(SessionFactory sessionFactory) {
        super(sessionFactory);

    }

    @Override
    public Student read(Serializable id) {
        Transaction transaction = null;
        Session session = sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            Student object = session.get(Student.class, id);
            session.getTransaction().commit();
            return object;

        } catch (RuntimeException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new RuntimeException(e.getClass() + ": cannot read instance with" + id.toString());
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Student> findByName(String name) {
        sessionFactory.getCurrentSession().enableFilter("nameFilter").setParameter("name", name);
        return sessionFactory.getCurrentSession().createQuery("from Student").list();
    }

    @SuppressWarnings("unchecked")
    public List<Student> findByFaculty(String name) {
        sessionFactory.getCurrentSession().enableFilter("facultyFilter").setParameter("faculty", name);
        return sessionFactory.getCurrentSession().createQuery("from Student").list();
    }
}
