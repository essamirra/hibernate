package com.netcracker.unc.hibernate.practice.datamodel;

import javax.persistence.*;

@MappedSuperclass
public class BaseEntity {
    protected Integer id;

    protected String name;

    public BaseEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
