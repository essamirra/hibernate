package com.netcracker.unc.hibernate.practice.persistence.impl;

import com.netcracker.unc.hibernate.practice.persistence.DAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.NotYetImplementedException;

import java.io.Serializable;

public class GenericDAOImpl<T> implements DAO<T> {
    protected SessionFactory sessionFactory;


    public GenericDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Serializable create(T instance) {
        Transaction transaction = null;
        Session session = sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            Serializable id = session.save(instance);
            session.getTransaction().commit();
            return id;
        } catch (RuntimeException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw new RuntimeException(e.getClass() + ": cannot create instance" + instance.toString());
        } finally {
            session.close();
        }

    }

    public T read(Serializable id) {
        throw new NotYetImplementedException();
    }


    public void update(T transientObject) {
        Transaction transaction = null;
        Session session = sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.update(transientObject);
            //session.merge(transientObject);
            session.getTransaction().commit();

        } catch (RuntimeException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new RuntimeException(e.getClass() + e.getMessage() + ", cannot update instance" + transientObject.toString());
        } finally {

            session.close();
        }
    }

    public void delete(T persistentObject) {
        Transaction transaction = null;
        Session session = sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.delete(persistentObject);
            session.getTransaction().commit();

        } catch (RuntimeException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new RuntimeException(e.getClass() + e.getMessage() + ": cannot delete instance" + persistentObject.toString());
        } finally {

            session.close();
        }
    }
}
