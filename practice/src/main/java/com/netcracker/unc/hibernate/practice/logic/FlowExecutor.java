package com.netcracker.unc.hibernate.practice.logic;

import com.netcracker.unc.hibernate.practice.datamodel.Scholarship;
import com.netcracker.unc.hibernate.practice.datamodel.Student;
import com.netcracker.unc.hibernate.practice.persistence.impl.ScholarshipDAO;
import com.netcracker.unc.hibernate.practice.persistence.impl.StudentDAO;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Date;

public class FlowExecutor {
    private SessionFactory sessionFactory;
    private StudentDAO studentDAO;
    private ScholarshipDAO scholarshipDAO;

    public FlowExecutor() {
        sessionFactory = new Configuration().configure().buildSessionFactory();
        studentDAO = new StudentDAO(sessionFactory);
        scholarshipDAO = new ScholarshipDAO(sessionFactory);
    }

    public Integer addStudent(String name, String faculty) {
        Student student = new Student();
        student.setName(name);
        student.setFaculty(faculty);
        return (Integer) studentDAO.create(student);
    }

    public Integer addScholarship(int amount, Date from, Date to) {
        Scholarship scholarship = new Scholarship();
        scholarship.setAmount(amount);
        scholarship.setFrom(from);
        scholarship.setTo(to);
        return (Integer) scholarshipDAO.create(scholarship);
    }

    public void updateStudent(Student s) {
        studentDAO.update(s);
    }

    public void updateScholarship(Scholarship s) {
        scholarshipDAO.update(s);
    }

    public void deleteStudent(Student d) {
        studentDAO.delete(d);
    }

    public void deleteScholarship(Scholarship d) {
        scholarshipDAO.delete(d);
    }


    public Student getStudent(Integer id) {
        return studentDAO.read(id);
    }

    public Scholarship getScholarship(Integer id) {
        return scholarshipDAO.read(id);
    }
}
