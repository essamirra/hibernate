package com.netcracker.unc.hibernate.practice.datamodel;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

@FilterDef(name = "nameFilter", parameters = @ParamDef(name = "name", type = "string"))
@Filter(name = "nameFilter", condition = ":name == name")
@Entity
@Table
public class Student extends BaseEntity {
    private String faculty;

    private Scholarship scholarship;

    public Student() {
    }

    @Column
    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    @OneToOne(mappedBy = "granted", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    public Scholarship getScholarship() {
        return scholarship;
    }

    public void setScholarship(Scholarship scholarship) {
        this.scholarship = scholarship;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", faculty='" + faculty + '\'' +
                ", scholarship=" + scholarship +
                "} " + super.toString();
    }
}
