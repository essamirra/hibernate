package com.netcracker.unc.hibernate.practice;


import com.netcracker.unc.hibernate.practice.datamodel.Scholarship;
import com.netcracker.unc.hibernate.practice.datamodel.Student;
import com.netcracker.unc.hibernate.practice.logic.FlowExecutor;

import java.util.Date;

public class Main {

    public static void main(String[] args) {
        FlowExecutor executor = new FlowExecutor();

        Integer id = executor.addStudent("Alex Kozitsin", "Data Mining");

        Student s = executor.getStudent(id);
        System.out.println(s);
        Integer id1 = executor.addStudent("Alex Kozitsin1", "Data Mining");

        Student ss = executor.getStudent(id1);

        Integer scholarshipId = executor.addScholarship(2500, new Date(2017, 10, 10), new Date(2017, 10, 15));
        Scholarship sch = executor.getScholarship(scholarshipId);
        System.out.println(sch);

        s.setScholarship(sch);
        executor.updateStudent(s);
        System.out.println(s);

        ss.setScholarship(sch);
        executor.updateStudent(ss);
        s = executor.getStudent(id);
        System.out.println(s);

        sch.setName("President");
        executor.updateScholarship(sch);
        System.out.println(sch);


        executor.deleteScholarship(sch);

        executor.deleteStudent(s);
        System.out.println(s);
        s = executor.getStudent(id);
        System.out.println(s);
    }

}
